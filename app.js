import express from "express";
import { engine } from "express-handlebars";
import * as env from "dotenv";

env.config()
const app = express();

app.engine('.hbs', engine({ extname: '.hbs' }));
app.set('view engine', 'hbs');
app.set('views', './views');

app.get('/', (req, res) => {
    res.render('home');
});

app.listen(process.env.PORT || 3000);
console.log(`app is now listening on ${process.env.PORT || 3000}`);
